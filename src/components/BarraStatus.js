import React from 'react';
import {
    FaBatteryEmpty,
    FaBatteryQuarter,
    FaBatteryHalf,
    FaBatteryThreeQuarters,
    FaBatteryFull,
  } from "react-icons/fa";

//color uno #4DA1FF media
//color dos #FFD012 alta
//color tres #FF5959 urgente
//color cuatro #A0D76A baja

export const BarraStatus = ({status}) => {
  return (
    <div className="d-flex">
        {status === 2 ? (
            [<FaBatteryEmpty size={'1.5em'} style={{color: '#FF5959'}} />,
            <p className='pl-2'>Abierto</p>]
        ) : status === 3 ? (
            [<FaBatteryQuarter size={'1.5em'} style={{color: '#FFD012'}} />,
            <p className='pl-2'>En curso</p>]
        ) : status === 4? (
            [<FaBatteryFull size={'1.5em'} style={{color: '#A0D76A'}} />,
            <p className='pl-2'>resuelto</p>]
        ) : <p className='pl-2'>Cerrado</p>}
    </div>
    );
};
