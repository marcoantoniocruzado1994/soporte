import React from "react";
import { Link } from "react-router-dom";
import "./Breadcrumb.css";
import Tickets from "./Tickets";

const Breadcrumb = ({ totalTickets, title, subtitle }) => {
  return (
    <>
      <div className="d-flex justify-content-between ">
        <nav aria-label="breadcrumb" style={{padding:'5px'}}>
          <ol className="breadcrumb breadCrumb ">
            <li className="breadcrumb-item">
              <Link to="/dashboard/solicitud">Dashboard</Link>
            </li>
            <li className="breadcrumb-item " aria-current="page">
              {title}
            </li>
            {subtitle ? (
              <li className="breadcrumb-item " aria-current="page">
                {subtitle}
              </li>
            ) : null}
          </ol>
        </nav>
        {totalTickets ? <Tickets totalTickets={totalTickets} /> : null}
      </div>
      <div>
        <hr />
      </div>
    </>
  );
};

export default Breadcrumb;
