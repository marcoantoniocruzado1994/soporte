import './Modal.css';

const Modal = ({ handleClose, show, children,ticket }) => {
    const showHideClassName = show ? "modal display-block" : "modal display-none";
    console.log(ticket);

    return (
      <div className={showHideClassName}>
        <section className="modal-main">
          {children}
          <button type="button" onClick={handleClose}>
            Close
          </button>
        </section>
      </div>
    );
};

export default Modal;
