import React from "react";
import "./Tickets.css";
import { FaInfoCircle } from "react-icons/fa";

const Tickets = ({ totalTickets }) => {
  return (
    <>
      <div className="tickets">
        <strong className="tickets_text">Tickets encontrados :  {totalTickets}</strong>
      </div>
    </>
  );
};

export default Tickets;
