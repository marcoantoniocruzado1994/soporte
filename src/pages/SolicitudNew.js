import React, { useState } from "react";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { uploadFile } from "react-s3";
import "./SolicitudNew.css";
import axios from "axios";
import Cookies from "universal-cookie";
import ReactTooltip from "react-tooltip";
import Breadcrumb from "../components/Breadcrumb";
import { FaInfoCircle } from "react-icons/fa";

const cookies = new Cookies();
//esto para toda las paginas
//mensajes de alerta
const MySwal = withReactContent(Swal);

const SolicitudNew = () => {
  const session = cookies.get("data");

  const [nombre, setNombre] = useState(
    session.firstname + ", " + session.lastname
  );
  const [email, setEmail] = useState(session.email);
  const [tipo, setTipo] = useState("");
  const [prioridad, setPrioridad] = useState("");
  const [detalle, setDeatalle] = useState("");
  const [archivo, setArchivo] = useState("");
  const [descripcion, setDescripcion] = useState("");
  const [isArchivoExt, setIsArchivoExt] = useState(true);
  const [isMessageArchivo, setIsMessageArchivo] = useState("");

  const RegistarTicket = async (e) => {
    e.preventDefault();
    const config = {
      bucketName: process.env.REACT_APP_BUCKET_NAME,
      region: process.env.REACT_APP_REGION,
      accessKeyId: process.env.REACT_APP_ACCESS_KEY,
      secretAccessKey: process.env.REACT_APP_SECRET_KEY,
    };

    const { location } = await uploadFile(archivo, config);
    //const archivoConvertido = await converBase64(archivo)
    let request_category;
    if (tipo === "Requerimiento") {
      request_category = "1";
    } else {
      request_category = "2";
    }
    const URL =
      "https://mthhurndg7.execute-api.us-east-1.amazonaws.com/bpi/support/register-ticket";
    const data = {
      email: email,
      name: nombre,
      request_category: request_category,
      request_subcategory: detalle,
      request_platform: "Chatbot",
      description: descripcion,
      //base64File: archivoConvertido,
      urlFile: location,
      prioridad: prioridad,
    };
    //console.log(data);
    //en axios siempre  se pone la url luego la data y luego las cabeceras
    const respuesta = await axios
      .post(URL, data, {
        headers: {
          "Content-Type": "application/json",
          "x-api-key": "lsbr2PeLqi42e1WdaAmlR6x3yc82YAThaUuEcTqJ",
        },
      })
      .then(function (response) {
        return response.data.data;
      })
      .catch(function (error) {
        console.log(error);
      });

    if (respuesta.statusCode == 200) {
      MySwal.fire({
        title: "Felicitaciones, ticket y email enviado",
        text: `${session.firstname},${session.lastname}`,
        icon: "success",
        showConfirmButton: false,
        timer: 2000,
      });
    } else {
      MySwal.fire({
        icon: "error",
        title: "Error:",
        text: "Completa todos los campos",
      });
    }
  };

  const demo = (e) => {
    e.preventDefault();
    if(archivo === ""){
      alert("Completa todos los campos" )
    }else{
      const extension = archivo.name.split(".").pop();
      const peso = archivo.size;
      const validExtension = ["xlsx", "txt", "pdf", "png", "jpg", "jpeg"];

      if (peso <= 2000000) {
        if (validExtension.includes(extension)) {
          
          if(tipo === "" || prioridad === "" || detalle === "" || descripcion === ""){
            alert("Completa todos los campos");
          }else{
            RegistarTicket(e);
          }
          setIsArchivoExt(true);
        } else {
          setIsArchivoExt(false);
          setIsMessageArchivo("Archivo no permitido");
        }
      } else {
        setIsArchivoExt(false);
        setIsMessageArchivo("El archivo es muy pesado");
      }
    }   
  };

  return (
    <>
      <div className="container">
        <div className="row">
          <Breadcrumb title={"solicitud"} subtitle={"nueva"} />
        </div>
        <div className="row d-flex flex-column align-content-center justify-content-center">
          <div className="col-md-8">
            <div className="card ">
              <div className="card-header">
                <h3 >Nueva Solicitud</h3>
                <p style={{color:'red'}}>El archivo a adjuntar, subirlo con un nombre sin espaciados caracteres permitidos (- , _ ). <p > Ejemplo : mi-archivo.pdf  ó  mi_archivo.png</p></p>
              </div>
              <div className="card-body">
                <form className="">
                  <div className="form-group ">
                    <label for="nombre p-2">Nombre Completo</label>
                    <input
                      type="text"
                      className="form-control"
                      id="nombre"
                      value={nombre}
                      disabled
                      onChange={(e) => setNombre(e.target.value)}
                    />
                  </div>
                  <div className="form-group ">
                    <label for="correo p-2">Correo</label>
                    <input
                      type="text"
                      className="form-control"
                      id="correo"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </div>
                  <div className="row">
                    <div className="form-group col-md-6">
                      <div className="form-group ">
                        <label for="exampleFormControlSelect1">
                          <p data-tip data-for="tipoSolicitud">
                            
                            Tipo Solicitud
                            <ReactTooltip
                              id="tipoSolicitud"
                              type="dark"
                              effect="solid"
                            >
                              <p>
                                <strong>Requerimiento</strong>
                                <p>consepto</p>
                              </p>
                              <p>
                                <strong>Incidencia</strong>
                                <p>consepto</p>
                              </p>
                            </ReactTooltip>
                            <strong style={{ color: "red" }}> * </strong>
                          </p>
                        </label>
                        <select
                          id="tipo"
                          onChange={(e) => setTipo(e.target.value)}
                          className="form-select"
                        >
                          <option>Selecciona una opcion</option>
                          <option>Requerimiento</option>
                          <option>Incidencia</option>
                        </select>
                      </div>
                    </div>
                    <div className="form-group col-md-6">
                      <label for="exampleFormControlSelect1">
                        <p data-tip data-for="tipoPrioridad">
                          Prioridad
                          <ReactTooltip
                            id="tipoPrioridad"
                            type="dark"
                            effect="solid"
                          >
                            <p>
                              <strong>Urgente</strong>
                              <p>consepto</p>
                            </p>
                            <p>
                              <strong>Alta</strong>
                              <p>consepto</p>
                            </p>
                            <p>
                              <strong>Media</strong>
                              <p>consepto</p>
                            </p>
                            <p>
                              <strong>Baja</strong>
                              <p>consepto</p>
                            </p>
                          </ReactTooltip>
                          <strong style={{ color: "red" }}>*</strong>
                          <i
                            className="bi bi-bookmark-fill"
                            style={
                              prioridad === "Alta"
                                ? { color: "#FFD012" }
                                : prioridad === "Media"
                                ? { color: "#4DA1FF" }
                                : prioridad === "Baja"
                                ? { color: "#A0D76A" }
                                : prioridad === "Urgente"
                                ? { color: "#FF5959" }
                                : { color: "black" }
                            }
                          ></i>
                        </p>{" "}
                      </label>
                      <select
                        id="tipo"
                        onChange={(e) => setPrioridad(e.target.value)}
                        className="form-select"
                      >
                        <option>Selecciona una opcion</option>
                        <option style={{ color: "#FF5959" }}>Urgente</option>
                        <option style={{ color: "#FFD012" }}>Alta</option>
                        <option style={{ color: "#4DA1FF" }}>Media</option>
                        <option style={{ color: "#A0D76A" }}>Baja</option>
                      </select>
                    </div>
                  </div>
                  {tipo === "Requerimiento" && prioridad ? (
                    <div className="form-group ">
                      <label for="exampleFormControlSelect1">
                        Detalle de Solicitud{" "}
                        <strong style={{ color: "red" }}> * </strong>
                      </label>
                      <select
                        id="detalle"
                        onChange={(e) => setDeatalle(e.target.value)}
                        className="form-select"
                      >
                        <option>Selecciona una opcion</option>
                        <option>Incluir nuevas keywords</option>
                        <option>Agregar estilos</option>
                        <option>Incluir nuevos textos</option>
                        <option>Cambiar derivaciones</option>
                        <option>Modificar el árbol conversacional</option>
                        <option>Actualizar contenido</option>
                        <option>Seguimiento continuo de los SLAs</option>
                        <option>
                          Mejora en el look and Feel del Dashboard
                        </option>
                      </select>
                    </div>
                  ) : tipo === "Incidencia" && prioridad ? (
                    <div className="form-group ">
                      <label for="exampleFormControlSelect1">
                        Detalle de Solicitud{" "}
                        <strong style={{ color: "red" }}> * </strong>
                      </label>

                      <select
                        id="detalle"
                        onChange={(e) => setDeatalle(e.target.value)}
                        className="form-select"
                      >
                        <option>Selecciona una opcion</option>
                        <option>Interrupción del servicio</option>
                        <option>Errores en la gestión</option>
                        <option>Error en desarrollo</option>
                        <option>Error en token</option>
                        <option>Suscripciones no identificadas</option>
                      </select>
                    </div>
                  ) : null}
                  <div className="form-group ">
                    <p>
                      Adjuntar archivo{" "}
                      <FaInfoCircle data-tip data-for="registerTip" size={13} />
                      <ReactTooltip id="registerTip" type="dark" effect="solid">
                        <div className="d-flex justify-content-evenly">
                          <div className="p-2">
                            <strong>Archivos permitidos</strong>
                            <ul>
                              <li>jpg/jpeg/png</li>
                              <li>pdf</li>
                              <li>txt</li>
                              <li>xlsx</li>
                            </ul>
                          </div>
                          <div className="p-2">
                            <strong>Peso maximo de archivo</strong>
                            <ul>
                              <li>2MB</li>
                            </ul>
                          </div>
                        </div>
                      </ReactTooltip>
                      <strong style={{ color: "red" }}> ** </strong>
                    </p>
                    {isArchivoExt ? null : (
                      <p style={{ color: "red", fontSize: "11px" }}>
                        {isMessageArchivo}
                      </p>
                    )}
                    <input
                      id="archivo"
                      onChange={(e) => setArchivo(e.target.files[0])}
                      type="file"
                      className="form-control-file"
                    />
                  </div>
                  <hr />
                  <div className="form-group ">
                    <label for="exampleFormControlTextarea1">
                      <p data-tip data-for="descripcionSolicitud">
                        Descripcion de Solicitud
                        <ReactTooltip
                          id="descripcionSolicitud"
                          type="dark"
                          effect="solid"
                        >
                          <strong>descripcion</strong>
                        </ReactTooltip>
                        <strong style={{ color: "red" }}> * </strong>
                      </p>
                    </label>
                    <textarea
                      id="descripcion"
                      onChange={(e) => setDescripcion(e.target.value)}
                      className="form-control"
                      placeholder="Complementa tu requerimiento  o incidencia con algun comentario"
                      rows="5"
                      maxLength={300}
                    ></textarea>
                  </div>
                  <div className="form-group ">
                    <input
                      type="button"
                      onClick={demo}
                      className=" btn boton__new btn-primary btn-block form-control"
                      value="Enviar Solicitud"
                    />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <p
          className="pt-3"
          style={{ color: "red", fontSize: "11px", fontWeight: "bold" }}
        >
          (**) Archivos permitidos excel, pdf, txt, png, jpg, jpeg.
        </p>
        <p style={{ color: "red", fontSize: "11px", fontWeight: "bold" }}>
          (*, **) Campos obligatorios.
        </p>
      </div>
    </>
  );
};

export default SolicitudNew;
