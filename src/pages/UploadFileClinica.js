import React from "react";
import { useState } from "react";
//upload csv file to postgresql database

const UploadFileClinica = () => {
  const [fileClinica, setFileClinica] = useState("");

  const uploadFileToPostgresql = (e) => {
    e.preventDefault();
    //upload postgresql
    console.log("fileClinica", fileClinica);
    

  };

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-header">
                <h4>Subir Archivo Clinica</h4>
              </div>
              <div className="card-body">
                <form>
                  <div className="form-group">
                    <label for="archivoClinica">Seleccione el archivo</label>
                    <input
                      type="file"
                      onChange={(e) => setFileClinica(e.target.files[0])}
                      className="form-control-file"
                      id="fileClinica"
                    />
                  </div>
                  <button
                    type="submit"
                    className="btn btn-primary"
                    onClick={uploadFileToPostgresql}
                  >
                    Subir
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default UploadFileClinica;
