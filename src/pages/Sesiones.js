import React, { useState } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
//esto para toda las paginas
import Cookies from "universal-cookie";
import Breadcrumb from "../components/Breadcrumb";

const cookies = new Cookies();
//esto para toda las paginas
//mensajes de alerta
const MySwal = withReactContent(Swal);

const Sesiones = () => {
  const session = cookies.get("data");
  console.log(session);

  const [fecha, setFecha] = useState("");
  const [sesiones, setSesiones] = useState([]);
  const [proveedor, setProveedor] = useState("");
  const [sesionAleatoria, setSesionAleatoria] = useState([]);
  const [email, setEmail] = useState(session.email);
  const [nombre, setNombre] = useState(
    session.firstname + ", " + session.lastname
  );
  const [estado, setEstado] = useState(false);

  const seleccionar = async () => {
    if (fecha === "") {
      MySwal.fire({
        title: "Error:",
        text: `Selecciona una fecha`,
        icon: "error",
        timer: 2000,
      });
    } else {
      let URL =
        "https://mthhurndg7.execute-api.us-east-1.amazonaws.com/bpi/support/sessiones-days";
      let data = {
        dia: fecha,
        proveedor: proveedor,
      };
      const response = await axios
        .post(URL, data)
        .then((res) => {
          return res.data.data;
        })
        .catch((err) => {
          console.log(err);
        });

      setSesiones(response.filas);
      setSesionAleatoria(response.data);
      setEstado(true);
    }
  };

  const enviarSessiones = async () => {
    if (sesiones.length === 0) {
      MySwal.fire({
        title: "Error:",
        text: `Selecciona una fecha`,
        icon: "error",
        timer: 2000,
      });
    } else {
      //envio de correo una vez generada las 8 session_id
      const URL =
        "https://mthhurndg7.execute-api.us-east-1.amazonaws.com/bpi/sendemail";
      const data = {
        to: `${email}`,
        text:
          `Sesiones del dia: ${fecha} ` +
          "<br> " +
          sesionAleatoria[0].session_id +
          "  <br> " +
          sesionAleatoria[1].session_id +
          " <br>  " +
          sesionAleatoria[2].session_id +
          " <br>  " +
          sesionAleatoria[3].session_id +
          " <br>  " +
          sesionAleatoria[4].session_id +
          " <br>  " +
          sesionAleatoria[5].session_id +
          " <br>  " +
          sesionAleatoria[6].session_id +
          " <br>  " +
          sesionAleatoria[7].session_id,
        subject: "Sessiones a revisión",
        html: true,
      };
      axios
        .post(URL, data)
        .then(function (response) {
          console.log(response.data);
        })
        .catch(function (error) {
          console.log(error);
        });
      MySwal.fire({
        position: "top-end",
        icon: "success",
        html: `<p>Se enviaron las sesiones al correo </p> <br> <b> ${email}</b> `,
        showConfirmButton: false,
        timer: 3000,
      });
    }
  };

  return (
    <>
      <div className="container">
        <div className="row">
          <Breadcrumb title={"sesiones"} subtitle={""} />
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-header">
                <h3>Sesiones</h3>
                <p className="text-success">
                  {" "}
                  <strong>{nombre}</strong>, selecciona una fecha para ver las
                  sesiones. Recuerda que las sessiones disponibles son hasta un
                  dia antes de la fecha actual.
                </p>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="form-group">
                    <label className="form-label">Fecha</label>
                    <input
                      class="form-control"
                      type="date"
                      onChange={(e) => setFecha(e.target.value)}
                      name="fecha"
                    />
                  </div>
                  <div className="form-group">
                    <label className="form-label">Proveedor</label>
                    <select
                      class="form-control"
                      name="proveedor"
                      id="proveedor"
                      onChange={(e) => setProveedor(e.target.value)}
                    >
                      <option value="">Selecciona un proveedor</option>
                      <option value="Chatter">Chatter RPA</option>
                      <option value="Chatbot">Chatboot RPA</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <button
                      className="btn btn-primary btn-block"
                      onClick={seleccionar}
                    >
                      Seleccionar Sessiones
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-4 justify-content-evenly">
          <div className="col-md-4">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col" className="text-center">
                    Sessiones {sesiones.length}{" "}
                  </th>
                </tr>
              </thead>
              <tbody>
                {sesiones.length > 0
                  ? sesiones.map((item, index) => {
                      "use strict";
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td>{item.session_id}</td>
                        </tr>
                      );
                    })
                  : " no hay datos"}
              </tbody>
            </table>
          </div>
          <div className="col-md-4">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col" className="text-center">
                    Sessiones Aleatorias {sesionAleatoria.length}{" "}
                  </th>
                </tr>
              </thead>
              <tbody>
                {sesionAleatoria.length > 0
                  ? sesionAleatoria.map((item, index) => {
                      "use strict";
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td className="text-center">{item.session_id}</td>
                        </tr>
                      );
                    })
                  : " no hay datos"}
              </tbody>
            </table>
            <div className="row">
              {estado ? (
                <div className="card">
                  <div className="card-header">
                    <h5>Enviar las sessiones a tu correo</h5>
                  </div>
                  <div className="card-body">
                    <input
                      class="form-control"
                      type="email"
                      value={email}
                      disabled
                      onChange={(e) => setEmail(e.target.value)}
                    />
                    <button
                      className="btn btn-primary btn-block mt-3"
                      onClick={enviarSessiones}
                    >
                      Enviar
                    </button>
                  </div>
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Sesiones;
