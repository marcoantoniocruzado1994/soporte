import React, { useState } from "react";
import "./SolicitudStatus.css";
import Breadcrumb from "../components/Breadcrumb";
import { Base64 } from 'js-base64';
//esto para toda las paginas
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import Cookies from "universal-cookie";
import axios from "axios";
import moment from "moment";
import "bootstrap/dist/css/bootstrap.min.css";

const cookies = new Cookies();
const MySwal = withReactContent(Swal);
//esto para toda las paginas 8956

const SolicitudStatus = () => {
  const [numTicket, setNumTicket] = useState("");
  const [estado, setEstado] = useState("");
  const [fechaCreacion, setFechaCreacion] = useState("");
  const [fechaActualizacion, setFechaActualizacion] = useState("");
  const [tipo, setTipo] = useState("");
  const [detalle, setDetalle] = useState("");
  const [id, setId] = useState("");

  const session = cookies.get("data");

  const infoTicket = async (e) => {
    e.preventDefault();

    if (numTicket === "") {
      MySwal.fire({
        title: "Error",
        text: "Numero de ticket no existe o esta en blanco",
        icon: "error",
        showConfirmButton: false,
        timer: 1500,
      });
    } else {
      const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
      const dataTicket = await axios.get(
        `https://newaccount1606398527611.freshdesk.com/api/v2/tickets/${numTicket}`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization:
            "Basic " + hash,
            Cookie: "_x_w=41",
          },
        }
      );
      const { status, data } = dataTicket;
      console.log("dataTicket", dataTicket);

      if (status === 200) {
        MySwal.fire({
          title: "Verificando",
          icon: "success",
          showConfirmButton: false,
          timer: 1500,
        });
        setEstado(data.status);
        setFechaCreacion(data.created_at);
        setTipo(data.custom_fields.cf_support_category);
        setDetalle(data.custom_fields.cf_support_subcategory);
        setId(data.id);
        setFechaActualizacion(data.updated_at);
      } else {
        MySwal.fire({
          title: "Error",
          icon: "error",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    }
  };

  return (
    <>
      <div className="container ">
        <div className="row">
          <Breadcrumb title={"solicitud"} subtitle={"estado"} />
        </div>
        <div className="row d-flex flex-column align-content-center justify-content-center ">
          <div className="col-md-8">
            <div className="card">
              <div className="card-header">
                <h3>Estado de Solicitud</h3>
                <p style={{ color: "green" }}>
                  <b>{session.firstname}</b>, pega el codigo del ticket que
                  acabas de copiar (ctrl+v)
                  <p>
                    luego Selecciona <b>ver estado</b>
                  </p>
                </p>
              </div>
              <div className="card-body">
                <form className="">
                  <div className="form-group p-3">
                    <label for="nombre p-2">
                      <b># Número de Ticket</b>
                    </label>
                    <input
                      type="number"
                      required
                      className="form-control"
                      id="numTicket"
                      onChange={(e) => setNumTicket(e.target.value)}
                    />
                  </div>
                  <div className="form-group p-3">
                    <input
                      type="button"
                      onClick={infoTicket}
                      className=" boton__celeste btn btn-primary btn-block form-control"
                      value="Ver estado"
                    />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        {/* Rederizado del ticket */}

        {estado !== "" ? (
          <div className="row d-flex flex-column align-content-center justify-content-center">
            <div className="col-md-5">
              <div
                className={
                  estado == 2
                    ? "card mt-4 border-danger "
                    : estado == 3
                    ? "card mt-4 border-warning "
                    : "card mt-4 border-success "
                }
              >
                <div
                  className="card-header text-center "
                  style={
                    estado == 2
                      ? {
                          background: "#dc3545",
                          color: "white",
                          border: "1px solid #D9534F ",
                        }
                      : estado == 3
                      ? {
                          background: "#ffc107",
                          color: "white",
                          border: "1px solid #ffc107",
                        }
                      : {
                          background: "#198754",
                          color: "white",
                          border: "1px solid #198754",
                        }
                  }
                >
                  <p>
                    <b>Ticket #{id}</b>
                  </p>
                </div>
                <div className="card-body">
                  <form className="">
                    <div className="formulario form-group p-3">
                      <p>
                        Estado de ticket :
                        <span>
                          {" "}
                          {estado == 2
                            ? "Por Hacer"
                            : estado == 3
                            ? "En curso"
                            : "Cerrado"}{" "}
                        </span>{" "}
                      </p>
                      <p>
                        Fecha de creación :{" "}
                        <span>{moment(fechaCreacion).format("L")} </span>
                      </p>
                      <p>
                        Hora de creación :{" "}
                        <span>{moment(fechaCreacion).format("LTS")}</span>{" "}
                      </p>
                      <p>
                        Hora de{" "}
                        {estado == 2
                          ? "actualización"
                          : estado == 3
                          ? "actualización"
                          : "cierre"}{" "}
                        :{" "}
                        <span>{moment(fechaActualizacion).format("LTS")}</span>{" "}
                      </p>
                      <p>
                        Tipo de Solicitud : <span>{tipo} </span>
                      </p>
                      <p>
                        Detalle de solicitud :<span>{detalle} </span>{" "}
                      </p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    </>
  );
};

export default SolicitudStatus;
