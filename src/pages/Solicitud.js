import React, { Component } from "react";
import { FaSearch } from "react-icons/fa";
import { Base64 } from 'js-base64';
import "./Solicitud.css";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import axios from "axios";
//esto para toda las paginas
import Cookies from "universal-cookie";
import Parrafo from "../components/Parrafo";
import { BarraStatus } from "../components/BarraStatus";

import Breadcrumb from "../components/Breadcrumb";
import { Link } from "react-router-dom";

const cookies = new Cookies();
//esto para toda las paginas
//mensajes de alerta
const MySwal = withReactContent(Swal);

export default class Solicitud extends Component {
  constructor() {
    super();
    this.state = {
      info: null,
      results: null,
      totalTickets: null,
      estado: "1",
      categoria: "1",
      show: false,
    };
    this.buscar = this.buscar.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  async componentDidMount() {
    const url = `https://newaccount1606398527611.freshdesk.com/api/v2/search/tickets?query="(cf_support_category:%27Requerimiento%27 OR cf_support_category:%27Incidencia%27) AND cf_support_customer:%27elcomercio%27"`;
    const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
    const config = {
      method: "get",
      url: url,
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Basic " + hash,
        Cookie: "_x_w=41",
      },
    };
    const response = await axios(config);
    
    this.setState({
      info: response.status,
      results: response.data.results,
      totalTickets: response.data.total,
    });
  }

  handleChange(event) {
    const target = event.target;
    const { value, name } = target;
    this.setState({
      [name]: value,
    });
  }

  buscar = async (e) => {
    e.preventDefault();
    if (this.state.categoria == "1" && this.state.estado == "1") {
      /* alert("Todos los tickets"); */
      const url = `https://newaccount1606398527611.freshdesk.com/api/v2/search/tickets?query="(cf_support_category:%27Requerimiento%27 OR cf_support_category:%27Incidencia%27) AND cf_support_customer:%27elcomercio%27"`;
      const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
      const config = {
        method: "get",
        url: url,
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Basic " + hash,
          Cookie: "_x_w=41",
        },
      };
      const response = await axios(config);
      /*  console.log("respuesta", response); */
      this.setState({
        info: response.status,
        results: response.data.results,
        totalTickets: response.data.total,
      });
    } else if (this.state.categoria == "1" && this.state.estado == "2") {
      /*  alert("Todos los tickets de estado abierto"); */
      const url = `https://newaccount1606398527611.freshdesk.com/api/v2/search/tickets?query="(cf_support_category:%27Requerimiento%27 OR cf_support_category:%27Incidencia%27) AND cf_support_customer:%27elcomercio%27 AND status:2"`;
      const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
      const config = {
        method: "get",
        url: url,
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Basic " + hash,
          Cookie: "_x_w=41",
        },
      };
      const response = await axios(config);
      /*  console.log("respuesta", response); */
      this.setState({
        info: response.status,
        results: response.data.results,
        totalTickets: response.data.total,
      });
    } else if (this.state.categoria == "1" && this.state.estado == "3") {
      /* alert("Todos los tickets en curso"); */
      const url = `https://newaccount1606398527611.freshdesk.com/api/v2/search/tickets?query="(cf_support_category:%27Requerimiento%27 OR cf_support_category:%27Incidencia%27) AND cf_support_customer:%27elcomercio%27 AND status:3"`;
      const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
      const config = {
        method: "get",
        url: url,
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Basic " + hash,
          Cookie: "_x_w=41",
        },
      };
      const response = await axios(config);
      /*    console.log("respuesta", response); */
      this.setState({
        info: response.status,
        results: response.data.results,
        totalTickets: response.data.total,
      });
    } else if (this.state.categoria == "1" && this.state.estado == "4") {
      /*  alert("Todos los tickets de estado resuelto"); */
      const url = `https://newaccount1606398527611.freshdesk.com/api/v2/search/tickets?query="(cf_support_category:%27Requerimiento%27 OR cf_support_category:%27Incidencia%27) AND cf_support_customer:%27elcomercio%27 AND status:4"`;
      const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
      const config = {
        method: "get",
        url: url,
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Basic " + hash,
          Cookie: "_x_w=41",
        },
      };
      const response = await axios(config);
      /*  console.log("respuesta", response); */
      this.setState({
        info: response.status,
        results: response.data.results,
        totalTickets: response.data.total,
      });
    } else if (this.state.categoria == "2" && this.state.estado == "1") {
      /* alert("Todos los tickets de incidencias de todos los estados"); */
      const url = `https://newaccount1606398527611.freshdesk.com/api/v2/search/tickets?query="(cf_support_category:%27Incidencia%27) AND cf_support_customer:%27elcomercio%27 "`;
      const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
      const config = {
        method: "get",
        url: url,
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Basic " + hash,
          Cookie: "_x_w=41",
        },
      };
      const response = await axios(config);
      /* console.log("respuesta", response); */
      this.setState({
        info: response.status,
        results: response.data.results,
        totalTickets: response.data.total,
      });
    } else if (this.state.categoria == "2" && this.state.estado == "2") {
      /*   alert("Todos los tickets de incidencias de estado abierto"); */
      const url = `https://newaccount1606398527611.freshdesk.com/api/v2/search/tickets?query="(cf_support_category:%27Incidencia%27) AND cf_support_customer:%27elcomercio%27 AND status:2"`;
      const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
      const config = {
        method: "get",
        url: url,
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Basic " + hash,
          Cookie: "_x_w=41",
        },
      };
      const response = await axios(config);
      /*  console.log("respuesta", response); */
      this.setState({
        info: response.status,
        results: response.data.results,
        totalTickets: response.data.total,
      });
    } else if (this.state.categoria == "2" && this.state.estado == "3") {
      /* alert("Todos los tickets de incidencias en curso"); */
      const url = `https://newaccount1606398527611.freshdesk.com/api/v2/search/tickets?query="(cf_support_category:%27Incidencia%27) AND cf_support_customer:%27elcomercio%27 AND status:3"`;
      const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
      const config = {
        method: "get",
        url: url,
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Basic " + hash,
          Cookie: "_x_w=41",
        },
      };
      const response = await axios(config);
      /*  console.log("respuesta", response); */
      this.setState({
        info: response.status,
        results: response.data.results,
        totalTickets: response.data.total,
      });
    } else if (this.state.categoria == "2" && this.state.estado == "4") {
      /*   alert("Todos los tickets de incidencias de estado resuelto"); */
      const url = `https://newaccount1606398527611.freshdesk.com/api/v2/search/tickets?query="(cf_support_category:%27Incidencia%27) AND cf_support_customer:%27elcomercio%27 AND status:4"`;
      const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
      const config = {
        method: "get",
        url: url,
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Basic " + hash,
          Cookie: "_x_w=41",
        },
      };
      const response = await axios(config);
      /* console.log("respuesta", response); */
      this.setState({
        info: response.status,
        results: response.data.results,
        totalTickets: response.data.total,
      });
    } else if (this.state.categoria == "3" && this.state.estado == "1") {
      /* alert("Todos los tickets de requerimientos de todos los estados"); */
      const url = `https://newaccount1606398527611.freshdesk.com/api/v2/search/tickets?query="(cf_support_category:%27Requerimiento%27 ) AND cf_support_customer:%27elcomercio%27"`;
      const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
      const config = {
        method: "get",
        url: url,
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Basic " + hash,
          Cookie: "_x_w=41",
        },
      };
      const response = await axios(config);
      /* console.log("respuesta", response); */
      this.setState({
        info: response.status,
        results: response.data.results,
        totalTickets: response.data.total,
      });
    } else if (this.state.categoria == "3" && this.state.estado == "2") {
      /* alert("Todos los tickets de requerimientos de estado abierto"); */
      const url = `https://newaccount1606398527611.freshdesk.com/api/v2/search/tickets?query="(cf_support_category:%27Requerimiento%27 ) AND cf_support_customer:%27elcomercio%27 AND status:2"`;
      const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
      const config = {
        method: "get",
        url: url,
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Basic " + hash,
          Cookie: "_x_w=41",
        },
      };
      const response = await axios(config);
      /* console.log("respuesta", response); */
      this.setState({
        info: response.status,
        results: response.data.results,
        totalTickets: response.data.total,
      });
    } else if (this.state.categoria == "3" && this.state.estado == "3") {
      /*  alert("Todos los tickets de requerimientos en curso"); */
      const url = `https://newaccount1606398527611.freshdesk.com/api/v2/search/tickets?query="(cf_support_category:%27Requerimiento%27 ) AND cf_support_customer:%27elcomercio%27 AND status:3"`;
      const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
      const config = {
        method: "get",
        url: url,
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Basic " + hash,
          Cookie: "_x_w=41",
        },
      };
      const response = await axios(config);
      /*   console.log("respuesta", response); */
      this.setState({
        info: response.status,
        results: response.data.results,
        totalTickets: response.data.total,
      });
    } else if (this.state.categoria == "3" && this.state.estado == "4") {
      /*   alert("Todos los tickets de requerimientos de estado resuelto"); */
      const url = `https://newaccount1606398527611.freshdesk.com/api/v2/search/tickets?query="(cf_support_category:%27Requerimiento%27 ) AND cf_support_customer:%27elcomercio%27 AND status:4"`;
      const hash = Base64.encode(process.env.REACT_APP_CODEAPI_FRESHDESK);
      const config = {
        method: "get",
        url: url,
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Basic " + hash,
          Cookie: "_x_w=41",
        },
      };
      const response = await axios(config);
      /* console.log("respuesta", response); */
      this.setState({
        info: response.status,
        results: response.data.results,
        totalTickets: response.data.total,
      });
    }
  };

  render() {
    //aca van las funciones
    const { estado, categoria } = this.state;

    const confirmacion = () => {
      MySwal.fire({
        position: "center",
        icon: "success",
        title: "Codigo copiado",
        showConfirmButton: false,
        timer: 1500,
      });
    };

    return (
      <>
        <div className="container p-4">
          <div className="row">
            <Breadcrumb
              title={"solicitud"}
              subtitle={""}
              totalTickets={this.state.totalTickets}
            />
          </div>
          <div className="row my-4 justify-content-evenly">
            <div className="col-md-4 mb-3">
              <dt for="categoria" class="form-label">
                Categoria
              </dt>
              <select
                value={categoria}
                onChange={this.handleChange}
                name="categoria"
                className="form-select "
              >
                <option value="1">Todos</option>
                <option value="2">Incidencia</option>
                <option value="3">Requerimiento</option>
              </select>
            </div>
            <div className="col-md-4 mb-3">
              <dt for="estado" class="form-label">
                Estado
              </dt>
              <select
                value={estado}
                onChange={this.handleChange}
                name="estado"
                className="form-select "
              >
                <option value="1">Todos</option>
                <option value="2">Abierto</option>
                <option value="3">En curso</option>
                <option value="4">Resuelto</option>
              </select>
            </div>
            <div className="col-md-4  mb-3 d-flex align-items-end ">
              <button
                onClick={this.buscar}
                className="btn  btn-primary btn-block"
              >
                <FaSearch size={"1em"} /> <strong>Buscar</strong>{" "}
              </button>
            </div>
          </div>
          <div className="row">
            <div className="card-columns">
              {this.state.info === 200 ? (
                this.state.results.map((item, index) => {
                  return (
                    <>
                      <div
                        key={index}
                        className="card animated fadeInDown  shadow  p-0"
                      >
                        <div className="card-header">
                          <h2>{item.subject}</h2>
                          <div className="row">
                            <div className="col-md-6 col-sm-12">
                              <strong>Estado del ticket</strong>
                              <BarraStatus status={item.status} />
                            </div>
                            <div className="col-md-6 col-sm-12">
                              <strong>Ticket N° </strong>
                              <p onClick={() => alert(` hola ${item.id}`)}>
                                {item.id}
                              </p>
                            </div>
                          </div>
                          <div className="row">
                            <strong>Categoria de solicitud</strong>
                            <p>{item.custom_fields.cf_support_category}</p>
                          </div>
                        </div>
                        <div className="card-body">
                          <Parrafo key={index} description={item.description} />
                          <hr />
                          <div className="row">
                            <button
                              onClick={() => {
                                navigator.clipboard.writeText(item.id);
                                confirmacion();
                              }}
  
                              className={
                                item.status === 2
                                  ? "btn  btn-danger ticket__abierto"
                                  : item.status === 3
                                  ? "btn btn-warning ticket__pendiente"
                                  : "btn btn-success ticket__resuelto"
                              }
                            >
                              <Link
                                style={{ textDecoration: "none" }}
                                className="text-white"
                                to="/dashboard/solicitud/estado-solicitud"
                              >
                                Ver ticket <i className="fas fa-eye"></i>
                              </Link>
                            </button>
                          </div>
                        </div>
                      </div>
                    </>
                  );
                })
              ) : (
                <div className="spinner-border text-primary" role="status">
                  <span className="visually-hidden">Loading...</span>
                </div>
              )}
            </div>
          </div>
        </div>
      </>
    );
  }
}
